package tudor.springframework.sfgpetclinic.services.map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tudor.springframework.sfgpetclinic.model.PetType;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PetTypeMapServiceTest {

    PetTypeMapService petTypeMapService;
    Long typeId = 1L;

    @BeforeEach
    void setUp() {
        petTypeMapService = new PetTypeMapService();
        PetType dogType = new PetType();
        dogType.setId(typeId);
        petTypeMapService.save(dogType);
    }

    @Test
    void findAll() {
        Set<PetType> types= petTypeMapService.findAll();
        assertEquals(1,types.size());
    }

    @Test
    void deleteById() {
        petTypeMapService.deleteById(typeId);
        assertEquals(0,petTypeMapService.findAll().size());
    }

    @Test
    void delete() {
        petTypeMapService.delete(petTypeMapService.findById(typeId));
        assertEquals(0,petTypeMapService.findAll().size());
    }

    @Test
    void save() {
        PetType type2=new PetType();
        type2.setId(2L);
        petTypeMapService.save(type2);
        assertEquals(2,petTypeMapService.findAll().size());
    }
    @Test
    void saveNoId(){
        PetType type3=new PetType();
        petTypeMapService.save(type3);
        assertNotNull(type3.getId());
        assertEquals(2,petTypeMapService.findAll().size());
    }

    @Test
    void findById() {
        PetType readType=petTypeMapService.findById(typeId);
        assertEquals(typeId,readType.getId());
    }
}