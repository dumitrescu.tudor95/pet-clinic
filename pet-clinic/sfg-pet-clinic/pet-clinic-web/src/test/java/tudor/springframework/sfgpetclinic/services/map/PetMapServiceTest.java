package tudor.springframework.sfgpetclinic.services.map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tudor.springframework.sfgpetclinic.model.Pet;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PetMapServiceTest {

    PetMapService petMapService;
    Long petId = 1L;

    @BeforeEach
    public void setUp() {
        petMapService = new PetMapService();
        Pet pet = new Pet();
        pet.setId(petId);
        petMapService.save(pet);
    }

    @Test
    void findAll() {
        Set<Pet> pets = petMapService.findAll();
        assertEquals(1, pets.size());
    }

    @Test
    void deleteById() {
        petMapService.deleteById(petId);
        assertEquals(0, petMapService.findAll().size());
    }

    @Test
    void delete() {
        petMapService.delete(petMapService.findById(petId));
        assertEquals(0, petMapService.findAll().size());
    }

    @Test
    void save() {
        Pet pet2 = new Pet();
        pet2.setId(2L);
        Pet savedPet2 = petMapService.save(pet2);
        assertNotNull(savedPet2);
        assertEquals(2, petMapService.findAll().size());
    }

    @Test
    void saveNoId() {
        Pet pet3 = new Pet();
        Pet savedPet3 = petMapService.save(pet3);
        assertNotNull(savedPet3);
        assertNotNull(savedPet3.getId());
    }

    @Test
    void findById() {
        Pet readPet=petMapService.findById(petId);
        assertEquals(petId,readPet.getId());
    }
}