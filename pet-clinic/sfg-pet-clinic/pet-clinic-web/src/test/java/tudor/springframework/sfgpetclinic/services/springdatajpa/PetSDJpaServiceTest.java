package tudor.springframework.sfgpetclinic.services.springdatajpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tudor.springframework.sfgpetclinic.model.Pet;
import tudor.springframework.sfgpetclinic.repositories.PetRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PetSDJpaServiceTest {

    @Mock
    PetRepository petRepository;

    @InjectMocks
    PetSDJpaService service;

    Pet returnPet;
    Long petId = 1L;

    @BeforeEach
    void setUp() {
        returnPet = new Pet();
        returnPet.setId(petId);
    }

    @Test
    void findAll() {
        Set<Pet> pets = new HashSet<>();
        Pet pet2 = new Pet();
        pet2.setId(2L);
        pets.add(returnPet);
        pets.add(pet2);

        when(petRepository.findAll()).thenReturn(pets);

        assertEquals(2, service.findAll().size());
        verify(petRepository, times(1)).findAll();
    }

    @Test
    void findById() {
        when(petRepository.findById(any())).thenReturn(Optional.of(returnPet));
        Pet readPet = service.findById(petId);
        assertNotNull(readPet);
        verify(petRepository, times(1)).findById(any());
    }


    @Test
    void findByIdNonExisting() {
        when(petRepository.findById(anyLong())).thenReturn(Optional.empty());
        Pet readPet = service.findById(10L);
        assertNull(readPet);
        verify(petRepository, times(1)).findById(any());
    }

    @Test
    void save() {
        Pet petToBeSaved=new Pet();

        when(service.save(any())).thenReturn(petToBeSaved);

        Pet savedPet=service.save(petToBeSaved);

        assertNotNull(savedPet);

        verify(petRepository,times(1)).save(any());
    }

    @Test
    void delete() {
        service.delete(returnPet);
        verify(petRepository,times(1)).delete(any());
    }

    @Test
    void deleteById() {
        service.deleteById(petId);
        verify(petRepository,times(1)).deleteById(anyLong());
    }
}