package tudor.springframework.sfgpetclinic.services.springdatajpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tudor.springframework.sfgpetclinic.model.Vet;
import tudor.springframework.sfgpetclinic.repositories.VetRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VetSDJpaServiceTest {

    @Mock
    VetRepository vetRepository;

    @InjectMocks
    VetSDJpaService service;

    Vet returnVet;
    Long vetId;

    @BeforeEach
    void setUp() {
        returnVet = new Vet();
        returnVet.setId(vetId);
    }

    @Test
    void findAll() {
        Set<Vet> vets=new HashSet<>();
        Vet vet2=new Vet();
        vet2.setId(2L);
        vets.add(returnVet);
        vets.add(vet2);

        when(vetRepository.findAll()).thenReturn(vets);

        assertEquals(2,service.findAll().size());
        verify(vetRepository,times(1)).findAll();
    }

    @Test
    void findById() {
        when(vetRepository.findById(any())).thenReturn(Optional.of(returnVet));

        assertNotNull(service.findById(vetId));
        verify(vetRepository,times(1)).findById(any());
    }

    @Test
    void findByIdNotExisting() {
        when(vetRepository.findById(any())).thenReturn(Optional.empty());

        assertNull(service.findById(vetId));
        verify(vetRepository,times(1)).findById(any());
    }

    @Test
    void save() {
        Vet vetToBeSaved =new Vet();
        vetToBeSaved.setId(2L);

        when(vetRepository.save(any())).thenReturn(vetToBeSaved);

        Vet savedVet=service.save(vetToBeSaved);

        assertNotNull(savedVet);
        verify(vetRepository,times(1)).save(any());
    }

    @Test
    void delete() {
    service.delete(returnVet);
    verify(vetRepository,times(1)).delete(any());
    }

    @Test
    void deleteById() {
        service.deleteById(vetId);
        verify(vetRepository,times(1)).deleteById(any());
    }
}