package tudor.springframework.sfgpetclinic.services.springdatajpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tudor.springframework.sfgpetclinic.model.Speciality;
import tudor.springframework.sfgpetclinic.repositories.SpecialtyRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SpecialitySDJpaServiceTest {

    @Mock
    SpecialtyRepository specialityRepository;

    @InjectMocks
    SpecialitySDJpaService service;

    Speciality returnSpeciality;
    Long specialityId;


    @BeforeEach
    void setUp() {
        returnSpeciality = new Speciality();
        returnSpeciality.setId(specialityId);
    }

    @Test
    void findAll() {
        Set<Speciality> specialities = new HashSet<>();
        Speciality speciality2 = new Speciality();
        speciality2.setId(2L);
        specialities.add(returnSpeciality);
        specialities.add(speciality2);

        when(specialityRepository.findAll()).thenReturn(specialities);

        assertEquals(2, service.findAll().size());
        verify(specialityRepository, times(1)).findAll();
    }

    @Test
    void findById() {
        when(specialityRepository.findById(any())).thenReturn(Optional.of(returnSpeciality));
        Speciality returnedSpeciality = service.findById(specialityId);
        assertNotNull(returnedSpeciality);
    }

    @Test
    void findByIdNotExisting() {
        when(specialityRepository.findById(any())).thenReturn(Optional.empty());
        Speciality returnedSpeciality = service.findById(specialityId);
        assertNull(returnedSpeciality);
    }

    @Test
    void save() {
    Speciality specialityToSave=new Speciality();
    specialityToSave.setId(3L);

    when(specialityRepository.save(any())).thenReturn(specialityToSave);

    Speciality savedSpeciality=service.save(specialityToSave);

    assertNotNull(savedSpeciality);
    verify(specialityRepository,times(1)).save(any());
    }

    @Test
    void delete() {
        service.delete(returnSpeciality);
        verify(specialityRepository,times(1)).delete(any());
    }

    @Test
    void deleteById() {
        service.deleteById(specialityId);
        verify(specialityRepository,times(1)).deleteById(any());
    }
}