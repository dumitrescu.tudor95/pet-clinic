package tudor.springframework.sfgpetclinic.services.map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tudor.springframework.sfgpetclinic.model.Vet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class VetMapServiceTest {

    VetMapService vetMapService;
    Vet testVet;
    final Long vetId=1L;

    @BeforeEach
    void setUp() {
        vetMapService = new VetMapService(new SpecialityMapService());
        testVet = new Vet();
        testVet.setId(vetId);
        vetMapService.save(testVet);
    }

    @Test
    void findAll() {
        assertEquals(1, vetMapService.findAll().size());
    }

    @Test
    void deleteById() {
        vetMapService.deleteById(vetId);
        assertNull(vetMapService.findById(vetId));
    }

    @Test
    void delete() {
        vetMapService.delete(testVet);
        assertEquals(0, vetMapService.findAll().size());
    }

    @Test
    void save() {
        Long newId=2L;
        Vet vetToBeSaved=new Vet();
        vetToBeSaved.setId(newId);
        Vet savedVet=vetMapService.save(vetToBeSaved);
        assertEquals(newId,savedVet.getId());
    }

    @Test
    void saveNoId() {

        Vet vetToBeSaved=new Vet();

        Vet savedVet=vetMapService.save(vetToBeSaved);

        assertNotNull(savedVet.getId());
    }

    @Test
    void findById() {
        System.out.println(vetId);
        Vet foundVet=vetMapService.findById(vetId);
        assertEquals(vetId,foundVet.getId());
    }
}