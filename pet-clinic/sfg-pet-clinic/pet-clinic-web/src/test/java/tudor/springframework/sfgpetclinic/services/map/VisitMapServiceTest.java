package tudor.springframework.sfgpetclinic.services.map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tudor.springframework.sfgpetclinic.model.Owner;
import tudor.springframework.sfgpetclinic.model.Pet;
import tudor.springframework.sfgpetclinic.model.Visit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class VisitMapServiceTest {

    VisitMapService service;

    Visit visit;
    Long visitId=1L;

    @BeforeEach
    void setUp() {
        service = new VisitMapService();
        visit = new Visit();
        visit.setId(visitId);
        Pet pet = new Pet();
        pet.setId(10L);
        Owner owner = new Owner();
        owner.setId(20L);
        pet.setOwner(owner);
        visit.setPet(pet);

        service.save(visit);
    }

    @Test
    void findAll() {
        assertEquals(1, service.findAll().size());
    }

    @Test
    void deleteById() {
        service.deleteById(visitId);
        assertNull(service.findById(visitId));
    }

    @Test
    void delete() {
        service.delete(visit);
        assertNull(service.findById(visitId));
    }

    @Test
    void saveNoPet() {
        Visit newVisit = new Visit();
        RuntimeException thrown = assertThrows(RuntimeException.class, () -> service.save(newVisit), "Expected runtime exception");
        assertTrue(thrown.getMessage().equals("Invalid visit"));
    }

    @Test
    void saveNoPetId() {
        Visit newVisit = new Visit();
        Pet pet = new Pet();
        newVisit.setPet(pet);
        RuntimeException thrown = assertThrows(RuntimeException.class, () -> service.save(newVisit), "Expected runtime exception");
        assertTrue(thrown.getMessage().equals("Invalid visit"));
    }

    @Test
    void saveNoPetOwner() {
        Visit newVisit = new Visit();
        Pet pet = new Pet();
        pet.setId(100L);
        newVisit.setPet(pet);
        RuntimeException thrown = assertThrows(RuntimeException.class, () -> service.save(newVisit), "Expected runtime exception");
        assertTrue(thrown.getMessage().equals("Invalid visit"));
    }

    @Test
    void saveNoPetOwnerId() {
        Visit newVisit = new Visit();
        Pet pet = new Pet();
        Owner owner = new Owner();
        pet.setOwner(owner);
        newVisit.setPet(pet);
        RuntimeException thrown = assertThrows(RuntimeException.class, () -> service.save(newVisit), "Expected runtime exception");
        assertTrue(thrown.getMessage().equals("Invalid visit"));
    }

    @Test
    void saveSuccessfully() {


        Owner owner = new Owner();
        owner.setId(2L);

        Pet pet = new Pet();
        pet.setId(4L);
        pet.setOwner(owner);

        Visit newVisit = new Visit();
        Long newVisitId = 5L;
        newVisit.setId(newVisitId);
        newVisit.setPet(pet);

        service.save(newVisit);

        assertEquals(newVisitId, service.findById(newVisitId).getId());
    }

    @Test
    void findById() {
        Visit foundVisit=service.findById(visitId);
        assertEquals(visitId,foundVisit.getId());
    }
}