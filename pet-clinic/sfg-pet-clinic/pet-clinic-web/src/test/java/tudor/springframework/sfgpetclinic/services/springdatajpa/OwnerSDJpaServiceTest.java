package tudor.springframework.sfgpetclinic.services.springdatajpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tudor.springframework.sfgpetclinic.model.Owner;
import tudor.springframework.sfgpetclinic.repositories.OwnerRepository;
import tudor.springframework.sfgpetclinic.repositories.PetRepository;
import tudor.springframework.sfgpetclinic.repositories.PetTypeRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OwnerSDJpaServiceTest {
    @Mock
    OwnerRepository ownerRepository;
    @Mock
    PetRepository petRepository;
    @Mock
    PetTypeRepository petTypeRepository;

    @InjectMocks
    OwnerSDJpaService service;

    Owner returnOwner;
    final String lastName = "Smith";
    final Long ownerId = 1L;

    @BeforeEach
    void setUp() {
        returnOwner=new Owner();
        returnOwner.setId(ownerId);
        returnOwner.setLastName(lastName);

    }

    @Test
    void findByLastName() {
        when(ownerRepository.findByLastName(any())).thenReturn(returnOwner);
        Owner smith = service.findByLastName(lastName);
        assertEquals(lastName, smith.getLastName());

        verify(ownerRepository).findByLastName(any());
    }

    @Test
    void findAll() {
        //given
        Set<Owner> allOwners = new HashSet<>();
        Owner owner1 = new Owner();
        owner1.setId(1L);
        Owner owner2 = new Owner();
        owner2.setId(2L);
        allOwners.add(returnOwner);
        allOwners.add(owner2);

        //when
        when(ownerRepository.findAll()).thenReturn(allOwners);

        //then
        assertNotNull(service.findAll());
        assertEquals(2,service.findAll().size());
    }

    @Test
    void findById() {

        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(returnOwner));

        Owner readOwner=service.findById(1L);
        assertNotNull(readOwner);
    }
    @Test
    void findByIdNotExisting() {

        when(ownerRepository.findById(anyLong())).thenReturn(Optional.empty());

        Owner readOwner=service.findById(1L);
        assertNull(readOwner);
    }

    @Test
    void save() {
        Owner ownerToSave=new Owner();
        ownerToSave.setId(10L);

        when(ownerRepository.save(any())).thenReturn(ownerToSave);

        assertNotNull(service.save(ownerToSave));

        verify(ownerRepository).save(any());
    }


    @Test
    void delete() {
        service.delete(returnOwner);
        verify(ownerRepository,times(1)).delete(any());
    }

    @Test
    void deleteById() {
        service.deleteById(ownerId);
        verify(ownerRepository,times(1)).deleteById(anyLong());
    }
}