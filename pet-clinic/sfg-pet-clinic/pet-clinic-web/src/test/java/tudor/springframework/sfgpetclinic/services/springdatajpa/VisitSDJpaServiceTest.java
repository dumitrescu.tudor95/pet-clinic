package tudor.springframework.sfgpetclinic.services.springdatajpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tudor.springframework.sfgpetclinic.model.Visit;
import tudor.springframework.sfgpetclinic.repositories.VisitRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VisitSDJpaServiceTest {

    @Mock
    VisitRepository visitRepository;

    @InjectMocks
    VisitSDJpaService service;

    Visit returnVisit;
    Long visitId;

    @BeforeEach
    void setUp() {
    returnVisit=new Visit();
    returnVisit.setId(visitId);
    }

    @Test
    void findAll() {
        Set<Visit> visits=new HashSet<>();
        Visit visit2=new Visit();
        visit2.setId(2L);
        visits.add(returnVisit);
        visits.add(visit2);

        when(visitRepository.findAll()).thenReturn(visits);


        assertEquals(2,service.findAll().size());
        verify(visitRepository,times(1)).findAll();
    }

    @Test
    void findById() {
        when(visitRepository.findById(any())).thenReturn(Optional.of(returnVisit));
        assertNotNull(service.findById(visitId));
        verify(visitRepository,times(1)).findById(any());
    }

    @Test
    void findByIdNotExisting() {
        when(visitRepository.findById(any())).thenReturn(Optional.empty());
        assertNull(service.findById(visitId));
        verify(visitRepository,times(1)).findById(any());
    }

    @Test
    void save() {
        Visit visitToBeSaved=new Visit();
        visitToBeSaved.setId(2L);

        when(visitRepository.save(any())).thenReturn(visitToBeSaved);

        assertNotNull(service.save(visitToBeSaved));
        verify(visitRepository,times(1)).save(any());
    }

    @Test
    void delete() {
        service.delete(returnVisit);
        verify(visitRepository,times(1)).delete(any());
    }

    @Test
    void deleteById() {
        service.deleteById(visitId);
        verify(visitRepository,times(1)).deleteById(any());
    }
}