package tudor.springframework.sfgpetclinic.services.map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tudor.springframework.sfgpetclinic.model.Speciality;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class SpecialityMapServiceTest {

    SpecialityMapService specialityMapService;

    Speciality testSpeciality;
    final Long specialityId = 1L;

    @BeforeEach
    public void init() {
        specialityMapService = new SpecialityMapService();
        testSpeciality = new Speciality();
        testSpeciality.setId(specialityId);
        specialityMapService.save(testSpeciality);
    }

    @Test
    void findAll() {
        Set<Speciality> specialities = specialityMapService.findAll();
        assertEquals(1, specialities.size());
    }

    @Test
    void deleteById() {
        specialityMapService.deleteById(specialityId);
        assertEquals(0, specialityMapService.findAll().size());
    }

    @Test
    void delete() {
        specialityMapService.delete(testSpeciality);
        assertEquals(0, specialityMapService.findAll().size());
    }

    @Test
    void saveExistingId() {
        Long specificId = 2L;
        Speciality speciality2 = new Speciality();
        speciality2.setId(specificId);
        Speciality savedSpeciality = specialityMapService.save(speciality2);

        assertNotNull(savedSpeciality);
        assertEquals(2, specialityMapService.findAll().size());
    }

    @Test
    void saveNonExstingId() {
        Speciality randomSpeciality = new Speciality();
        Speciality savedSpeciality = specialityMapService.save(randomSpeciality);
        assertNotNull(savedSpeciality);
        assertNotNull(savedSpeciality.getId());
    }

    @Test
    void findById() {
        Speciality test = specialityMapService.findById(specialityId);
        assertEquals(test.getId(), specialityId);
    }
}
