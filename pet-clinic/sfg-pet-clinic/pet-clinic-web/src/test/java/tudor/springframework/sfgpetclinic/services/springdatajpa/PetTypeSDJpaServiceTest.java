package tudor.springframework.sfgpetclinic.services.springdatajpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import tudor.springframework.sfgpetclinic.model.PetType;
import tudor.springframework.sfgpetclinic.repositories.PetTypeRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PetTypeSDJpaServiceTest {

    @Mock
    PetTypeRepository petTypeRepository;

    @InjectMocks
    PetTypeSDJpaService service;

    PetType returnType;
    Long typeId=1L;

    @BeforeEach
    void setUp() {
    returnType =new PetType();
    returnType.setId(typeId);
    }

    @Test
    void findAll() {
        Set<PetType> types=new HashSet<>();
        types.add(returnType);
        PetType secondType=new PetType();
        secondType.setId(2L);
        types.add(secondType);

        when(petTypeRepository.findAll()).thenReturn(types);

        assertEquals(2,service.findAll().size());
        verify(petTypeRepository,times(1)).findAll();
    }

    @Test
    void findById() {
        when(petTypeRepository.findById(any())).thenReturn(Optional.of(returnType));
        PetType foundType=service.findById(typeId);
        assertNotNull(foundType);
    }
    @Test
    void findByIdNotExisting() {
        when(petTypeRepository.findById(any())).thenReturn(Optional.empty());
        PetType foundType=service.findById(typeId);
        assertNull(foundType);
    }

    @Test
    void save() {
        PetType savedPetType=new PetType();
        savedPetType.setId(2L);

        when(petTypeRepository.save(any())).thenReturn(savedPetType);

        PetType readType=service.save(savedPetType);

        assertNotNull(readType);
        verify(petTypeRepository,times(1)).save(any());
    }

    @Test
    void delete() {
        service.delete(returnType);
        verify(petTypeRepository,times(1)).delete(any());
    }

    @Test
    void deleteById() {
        service.deleteById(typeId);
        verify(petTypeRepository,times(1)).deleteById(anyLong());
    }
}