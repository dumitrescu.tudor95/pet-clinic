package tudor.springframework.sfgpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import tudor.springframework.sfgpetclinic.model.PetType;

public interface PetTypeRepository extends CrudRepository<PetType,Long> {
}
