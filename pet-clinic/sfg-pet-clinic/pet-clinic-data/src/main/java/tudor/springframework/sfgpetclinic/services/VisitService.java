package tudor.springframework.sfgpetclinic.services;

import tudor.springframework.sfgpetclinic.model.Visit;

public interface VisitService extends CrudService<Visit, Long> {
}
