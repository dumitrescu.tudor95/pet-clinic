package tudor.springframework.sfgpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import tudor.springframework.sfgpetclinic.model.Pet;

public interface PetRepository extends CrudRepository<Pet,Long> {
}
