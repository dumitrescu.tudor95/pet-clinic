package tudor.springframework.sfgpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import tudor.springframework.sfgpetclinic.model.Visit;

public interface VisitRepository extends CrudRepository<Visit,Long> {
}
