package tudor.springframework.sfgpetclinic.services;

import tudor.springframework.sfgpetclinic.model.Vet;

import java.util.Set;

public interface VetService extends CrudService<Vet,Long> {
}
