package tudor.springframework.sfgpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import tudor.springframework.sfgpetclinic.model.Speciality;

public interface SpecialtyRepository extends CrudRepository<Speciality,Long> {
}
