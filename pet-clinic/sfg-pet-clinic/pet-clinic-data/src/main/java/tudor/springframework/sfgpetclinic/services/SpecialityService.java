package tudor.springframework.sfgpetclinic.services;

import tudor.springframework.sfgpetclinic.model.Speciality;
import tudor.springframework.sfgpetclinic.services.CrudService;

public interface SpecialityService extends CrudService<Speciality,Long> {
}
