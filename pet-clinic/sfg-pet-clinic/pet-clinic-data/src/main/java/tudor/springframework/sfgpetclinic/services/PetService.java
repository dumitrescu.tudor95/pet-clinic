package tudor.springframework.sfgpetclinic.services;

import tudor.springframework.sfgpetclinic.model.Pet;
import tudor.springframework.sfgpetclinic.model.PetType;


public interface PetService extends CrudService<Pet,Long> {

}
