package tudor.springframework.sfgpetclinic.services;

import tudor.springframework.sfgpetclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType,Long> {
}
