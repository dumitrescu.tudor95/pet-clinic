package tudor.springframework.sfgpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import tudor.springframework.sfgpetclinic.model.Vet;

public interface VetRepository extends CrudRepository<Vet,Long> {
}
