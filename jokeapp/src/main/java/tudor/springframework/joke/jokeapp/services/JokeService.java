package tudor.springframework.joke.jokeapp.services;

public interface JokeService {

    String getJoke();
}
