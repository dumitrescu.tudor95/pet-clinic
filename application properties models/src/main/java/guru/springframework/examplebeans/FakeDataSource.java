package guru.springframework.examplebeans;


import lombok.Getter;
import lombok.Setter;

public class FakeDataSource {
  @Getter
  @Setter
    private String user;
    @Getter
    @Setter
    private String password;
    @Getter
    @Setter
    private String url;
}
